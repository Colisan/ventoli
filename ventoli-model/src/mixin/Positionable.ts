import { Constructor } from '.';

export default <TBase extends Constructor>(Base: TBase) =>
	class Positionable extends Base {
		private _x: number;
		private _y: number;
		private _z: number;
		private _zThird: number;

		get x(): number {
			return this._x;
		}
		set x(val: number) {
			this._x = val;
		}

		get y(): number {
			return this._y;
		}
		set y(val: number) {
			this._y = val;
		}

		get z(): number {
			return this._z;
		}
		set z(val: number) {
			this._z = val;
			this._zThird = val / 3;
		}

		distanceTo(target: Positionable): number {
			const dx = Math.abs(this.x - target.x);
			const dy = Math.abs(this.y - target.y);
			const dz = Math.floor(Math.abs(this._zThird - target._zThird));
			return dx + dy + dz;
		}
	};
