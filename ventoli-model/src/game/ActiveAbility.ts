import { Ability, Tile, UnitOnMap } from '..';

export default class ActiveAbility implements Ability {
	public physicalPower = 0;
	public magicalPower = 0;
	public isHealing = false;

	public sourceUnit: UnitOnMap;

	public isValidTarget(tile: Tile) {
		return tile === this.sourceUnit.currentTile;
	}

	public isInAoe(tile: Tile, withTarget: Tile) {
		return tile === withTarget;
	}
}
