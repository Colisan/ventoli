export class PopupButton {
	public label: string;
	public action: () => void;

	constructor(label: string, action: () => void) {
		this.label = label;
		this.action = action;
	}
}

export class Popup {
	public title?: string;
	public content: string;
	public buttonList: Array<PopupButton>;
	public createdIndex?: number;

	constructor(content: string) {
		this.content = content;
		this.buttonList = [];
	}
}
