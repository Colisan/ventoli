import { Router } from 'express';

export type RouteInfos = {
	method: keyof Pick<Router, 'get' | 'post' | 'put' | 'delete' | 'patch' | 'options' | 'head'>;
	url: string;
	needUserAuth: boolean;
	needServerAuth: boolean;
};

function getAvaliableRoutes<T extends Record<string, RouteInfos>>(routelist: T) {
	return routelist;
}

export const avaliableRoutes = getAvaliableRoutes({
	VALID_AUTH: { method: 'get', url: '/auth', needUserAuth: true, needServerAuth: false },
	POST_LOGIN: { method: 'post', url: '/auth/login', needUserAuth: false, needServerAuth: false },
	PUT_SELF_PLAYER: { method: 'put', url: '/player', needUserAuth: true, needServerAuth: false },
	GET_SELF_PLAYER: { method: 'get', url: '/player', needUserAuth: true, needServerAuth: false },
	POST_NEW_PLAYER: { method: 'post', url: '/player', needUserAuth: false, needServerAuth: false },
	GET_OTHER_PLAYER: {
		method: 'get',
		url: '/player/:playername',
		needUserAuth: false,
		needServerAuth: false,
	},
	POST_GAMESERVER: { method: 'post', url: '/server', needUserAuth: false, needServerAuth: true },
});

export type RouteName = keyof typeof avaliableRoutes;

export type RouteList = Record<RouteName, RouteInfos>;
