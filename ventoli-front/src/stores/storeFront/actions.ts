import { ActionContext } from 'vuex';
import { AxiosError, AxiosResponse } from 'axios';
import { getInitialState, State } from '@/stores/storeFront/state';
import { Player } from '@ventoli/ventoli-model';
import { Popup } from '@/model/Popup';
import { typedStore } from '@/stores/storeFront';
import callApi from '@/services/callApi';

type TypedActionContext = Omit<ActionContext<State, State>, 'getters' | 'commit' | 'dispatch'> & typedStore;

export default {
	async TestThenCallSetToken(context: TypedActionContext, token: string): Promise<void> {
		return callApi('VALID_AUTH', token).then(() => {
			context.commit('SetAuthToken', token);
		});
	},
	async CallLogin(
		context: TypedActionContext,
		credentials: { login: string; password: string; willRemember: boolean }
	): Promise<void> {
		return callApi(
			'POST_LOGIN',
			context.state.authToken,
			{},
			{
				playername: credentials.login,
				password: credentials.password,
				willRemember: credentials.willRemember,
			}
		).then((res: AxiosResponse) => {
			context.commit('SetAuthToken', res.data);
			context.dispatch('CallGetSelfAccount');
		});
	},
	async CallGetSelfAccount(context: TypedActionContext): Promise<void> {
		return callApi('GET_SELF_PLAYER', context.state.authToken).then((res: AxiosResponse<Player>) => {
			context.commit('SetCurrentPlayer', res.data);
		});
	},
	async CallCreateAccount(
		context: TypedActionContext,
		credentials: { login: string; password: string }
	): Promise<void> {
		return callApi('POST_NEW_PLAYER', context.state.authToken, {
			playername: credentials.login,
			password: credentials.password,
		})
			.then(() => {
				return;
			})
			.catch((err: AxiosError) => {
				if (err.response) {
					throw new Error(err.response.data);
				}
				throw new Error(err.toString());
			});
	},
	async CallEditSelfAccount(
		context: TypedActionContext,
		informations: { oldPassword: string; newPassword: string }
	): Promise<void> {
		if (context.state.currentPlayer)
			return callApi('PUT_SELF_PLAYER', context.state.authToken, {
				id: context.state.currentPlayer.id,
				oldPassword: informations.oldPassword,
				newPassword: informations.newPassword,
			})
				.then((res: AxiosResponse) => {
					return res.data;
				})
				.catch((err: AxiosError) => {
					console.error(err);
					if (err.response) {
						throw new Error(err.response.data);
					}
					throw new Error(err.toString());
				});
		else return Promise.reject('Not logged in');
	},
	Logout(context: TypedActionContext): void {
		const initialState = getInitialState();
		context.commit('SetAuthToken', initialState.authToken);
		context.commit('SetCurrentGame', initialState.currentGame);
		context.commit('SetCurrentPlayer', initialState.currentPlayer);
	},
	ShowInfoPopup(context: TypedActionContext, infos: { title?: string; content: string; okLabel?: string }): void {
		const popup = new Popup(infos.content);
		popup.title = infos.title;

		popup.buttonList = [
			{
				label: infos.okLabel ?? 'Ok',
				action: () => {
					if (popup.createdIndex) context.commit('RemovePopup', popup.createdIndex);
				},
			},
		];
		context.commit('AddPopup', popup);
		popup.createdIndex = context.state.lastPopupIndex.valueOf();
	},
};
