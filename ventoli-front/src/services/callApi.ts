import axios, { AxiosError, AxiosRequestConfig, AxiosResponse } from 'axios';
import { avaliableRoutes, RouteName } from '@ventoli/ventoli-api/src/route/routes';

export default async function (
	route: RouteName,
	token?: string,
	getParams: Record<string, { toString: () => string }> = {},
	postParams: Record<string, { toString: () => string }> = {}
): Promise<AxiosResponse> {
	const routeInfos = avaliableRoutes[route];

	const url = `${process.env.VUE_APP_VENTOLI_API_URL}${routeInfos.url}`;
	for (const name in getParams) {
		url.replace(`:${name}`, getParams[name].toString());
	}

	const handleApiError = (err: AxiosError) => {
		if (err.response) {
			throw new Error(err.response.data);
		}
		throw new Error(err.toString());
	};

	const config: AxiosRequestConfig = {};
	if (routeInfos.needUserAuth) {
		config.headers = {
			Authorization: `Bearer ${token}`,
		};
	}

	if (process.env.NODE_ENV === 'development') console.log('calling API', url, postParams, config);

	if (routeInfos.method === 'get') {
		return axios.get(url, config).catch(handleApiError);
	} else if (routeInfos.method === 'post') {
		return axios.post(url, postParams, config).catch(handleApiError);
	} else if (routeInfos.method === 'put') {
		return axios.put(url, postParams, config).catch(handleApiError);
	} else return Promise.reject('Wrong route method');
}
