import * as jwt from 'jsonwebtoken';
import { Player } from '..';

type PlayerAuthData = {
	playerid?: number;
	playername?: string;
};

type ServerAuthData = {
	serverType?: string;
};

type PayloadData = PlayerAuthData | ServerAuthData;

export default class JwtPayload<PayloadType extends PlayerAuthData | ServerAuthData> {
	private static jwtSecret: string;

	private datas: PayloadType;

	public noExpiration: boolean;

	constructor(datas?: PayloadType, noExpiration = false) {
		if (datas) this.datas = datas;
		this.noExpiration = noExpiration;
	}

	public static setJwtSecret(newSecret) {
		JwtPayload.jwtSecret = newSecret;
	}

	public getSignedToken(): string {
		if (JwtPayload.jwtSecret)
			return jwt.sign(this.datas, JwtPayload.jwtSecret, this.noExpiration ? {} : { expiresIn: '1h' });
		else throw 'No JwtSecret set in JwtPayload';
	}

	public static fromPlayerEntity(player: Player) {
		const datas: PlayerAuthData = {
			playerid: player.id,
			playername: player.name,
		};
		return new JwtPayload<PlayerAuthData>(datas);
	}

	public static fromPlayerSignedToken(token: string): JwtPayload<PlayerAuthData> {
		if (JwtPayload.jwtSecret)
			return new JwtPayload<PlayerAuthData>(jwt.verify(token, JwtPayload.jwtSecret) as PlayerAuthData);
		else throw 'No JwtSecret set in JwtPayload';
	}

	public static fromServerSignedToken(token: string): JwtPayload<ServerAuthData> {
		if (JwtPayload.jwtSecret) return new JwtPayload(jwt.verify(token, JwtPayload.jwtSecret) as ServerAuthData);
		else throw 'No JwtSecret set in JwtPayload';
	}
}
