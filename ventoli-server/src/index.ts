import axios from 'axios';
import WebSocket from 'ws';
import GameHandler from './messageHandlers/GameHandler';
import NetworkHandler from './messageHandlers/NetworkHandler';
import { onMessage } from './Protocol';
import { JwtPayload } from '@ventoli/ventoli-model';

const defaultLog = console.log;
console.log = (...args) => {
	defaultLog(`[${process.env.npm_package_name}]`, ...args);
};
console.log('api', process.env.VUE_APP_VENTOLI_API_URL);

JwtPayload.setJwtSecret(process.env.JWT_SECRET);

const testAPIToBeOnline = async (timeout: number): Promise<void> => {
	return new Promise((resolve, reject) => {
		const timeoutId = setTimeout(() => {
			reject();
		}, timeout);
		axios
			.get(process.env.VUE_APP_VENTOLI_API_URL)
			.then((_) => {
				clearTimeout(timeoutId);
				resolve();
			})
			.catch((_) => {
				reject();
			});
	});
};

const waitForAPIToBeOnline = async (): Promise<void> => {
	return new Promise(async (resolve, _) => {
		let expBackoff = 100;
		let isAPIOffline = false;
		while (isAPIOffline) {
			const waiting = testAPIToBeOnline(expBackoff)
				.then((_) => {
					console.log('API is online ! <3');
					isAPIOffline = true;
					resolve();
				})
				.catch((_) => {
					console.log('API still not online');
				});
			await waiting;
			expBackoff *= 1.5;
		}
	});
};

waitForAPIToBeOnline().then((_) => {
	const webSocketServer = new WebSocket.Server({ port: 4000 });

	webSocketServer.on('connection', (webSocket) => {
		webSocket.send('Hello! Message From Server!');

		const networkHandler = new NetworkHandler(webSocket);
		const gameHandler = new GameHandler(webSocket);

		onMessage(webSocket, (message) => {
			console.log(`${message.type} message recieved: ${JSON.stringify(message.payload)}`);
			networkHandler.handleMessage(message);
			gameHandler.handleMessage(message);
		});
	});
});
