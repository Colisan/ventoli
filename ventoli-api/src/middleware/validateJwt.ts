import { Request, Response, NextFunction } from 'express';
import { JwtPayload } from '@ventoli/ventoli-model';

/*
const getValidateJwt = <PayLoadClass>(
	PayLoadClass: typeof PlayerAuthJwtPayload | typeof ServerAuthJwtPayload
) => (req: Request, res: Response, next: NextFunction) => {
	let token: string | undefined;

	if (!(token = req.headers.authorization?.split(' ')[1])) {
		res.status(400).send('Require authentification token');
		return;
	}

	if (token) {
		let payload: typeof PayLoadClass;
		try {
			payload = PayLoadClass.fromSignedToken(token);
			res.locals.jwtPayload = payload;

			//res.setHeader('token', payload.getSignedToken());

			next();
		} catch (error) {
			res.status(401).send('Invalid authentification token');
			return;
		}
	}

	return;
};
*/

const getValidateJwt =
	(validate: (token: string) => JwtPayload<any>) => (req: Request, res: Response, next: NextFunction) => {
		let token: string | undefined;

		if (!(token = req.headers.authorization?.split(' ')[1])) {
			res.status(400).send('Require authentification token');
			return;
		}

		if (token) {
			let payload: JwtPayload<any>;
			try {
				payload = validate(token);
				res.locals.jwtPayload = payload;

				//res.setHeader('token', payload.getSignedToken());

				next();
			} catch (error) {
				res.status(401).send('Invalid authentification token');
				return;
			}
		}

		return;
	};

export const validatePlayerJwt = getValidateJwt((token: string) => {
	return JwtPayload.fromPlayerSignedToken(token);
});

export const validateServeJwt = getValidateJwt((token: string) => {
	return JwtPayload.fromServerSignedToken(token);
});
